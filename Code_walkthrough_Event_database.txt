Code walk through document
Event Database









































Code walk through document	1
Event Database	1
Architecture	2
Upload tool	3
Website	5
Web services	8
Database	9




The Event Management tool consists of the following components:

1. Upload tool
2. Webservices
3. Website
4. Databases (MongoDB & MySQL)














Architecture



Upload tool

The upload tool is built using Java. It is passed the input folder path. 
? It reads the path and extracts the video folders and the video chunks (videos of length of 5 minutes). 
? Java Files API, is used for iterating over the files in the input folder,
? 
? It uploads the video chunks to Mongodb and the video details into Mysql database.
? The upload of video chunk into MongoDB is done using mongofiles (https://www.tutorialspoint.com/mongodb/mongodb_gridfs.htm)
? 
? Upload to MySQL is done using JDBC (https://www.mkyong.com/jdbc/jdbc-preparestatement-example-update-a-record/)
? 

? The tool has an option for deleting all files (both MongoDB & MySQL) in the system:
? BE careful, this is only for testing purpose:
? Java -jar uploadtool.jar delete_everything
? 



Website

The website is written using Angularjs. It interacts with the web services to query and display videos and tags.

? The website has been built using the free bootstrap template, SB Admin 2 (https://startbootstrap.com/template-overviews/sb-admin-2/)
? The main files to focus in the code are in the app/scripts folder:
? 
? app/scripts/controllers folder has the controllers
? app/scripts/directives has the UI elements (sidebar, dashboard etc)
? HTML5 video player has been used to play the videos. (http://www.w3schools.com/html/html5_video.asp)
? Configuration of the website is stored in app/scripts/app.js file. The update to the IP address of the web service is done here:
? 
? Dependencies are managed using Bower (https://bower.io/) and npm (https://www.npmjs.com/).
? Grunt (http://gruntjs.com/) is used for managing the tasks.
? Based on the query parameters, videos are retrieved from the web services and queued for playing.
? AJAX requests are used to communicate with the WebServices. The data is sent in JSON format. (https://www.sitepoint.com/ajaxjquery-getjson-simple-example/)
? The code for fetching the videos from the WebServices based on the event ID is at loadvideosbyid() method in app/scripts/controllers/main.js file.
? 

? Once the query parameters are sent to the Webservice, the Webservice returns a list of video files matching the query. The list is populated $scope.videos model.
? Search Results window (app/scripts/directives/searchresult/searchresult.html) displays the data in the $scope.videos model
? 
? Similarly check the loadvideosbydate and loadvideosbytag for querying by event date and data.
? Play Video: On click of the video in the searchresult, the related video is fetched from the WebService, and played in the player:
? 
? Add tag: Video may be paused and a tag may be added to it
? On click of add tag button, the add-tag form is displayed
? Once the details are entered and save button is clicked, the data is sent to the web service for updation to the database:
? 
? View tag: Tags of the video are displayed in the tags window. On click, the video is played from the timestamp related to the tag.
? 





Web services

The web services are written using Nodejs. It uses Express framework for defining the web services (https://www.tutorialspoint.com/nodejs/nodejs_express_framework.htm). 

? The configuration is defined in config.js file.
? 
? The helper code is in helper.js file. 
? The web services uploads and downloads files from MongoDB using the mongofiles command. (https://www.tutorialspoint.com/mongodb/mongodb_gridfs.htm)
? To stream the video file over the network, it uses the gridfs-stream library (https://github.com/aheckmann/gridfs-stream). 
? The interaction with MySQL is through mysql library. (https://www.sitepoint.com/using-node-mysql-javascript-client/).

The following endpoints have been defined, and code is available in the server.js file.
 
1.         /tags (http://10.29.19.67:8888/tags), method=POST
       This is used to create tags
2.         /check (http://10.29.19.67:8888/check), method=GET
       This is used to check if provided video url exists in the database
3.         /load (http://10.29.19.67:8888/load), method=GET
       This returns the video contents (binary data) of the provided video name
4.         /savedetails (http://10.29.19.67:8888/savedetails), method=POST
       This is used to update video detail information
5.         /savecounts (http://10.29.19.67:8888/savecounts), method=POST
       This is used to create video related count information
6.         /tags (http://10.29.19.67:8888/tags), method=DELETE
       This is used to delete tags
7.         /login (http://10.29.19.67:8888/login), method=POST
       This is used to authenticate the login credentials
8.         /tags (http://10.29.19.67:8888/tags), method=POST
       This is used to create tags
9.         /searchbyid (http://10.29.19.67:8888/searchbyid), method=POST
 	Searched videos for the provided event ID
10.   /searchbytag (http://10.29.19.67:8888/searchbytag), method=POST
        	Searched videos for the provided values. All the values in the search information window
        	of the website are supported.
11.   /searchbydate (http://10.29.19.67:8888/searchbydate), method=POST
        	Searched videos for the provided event date
12.   /getentiretags (http://10.29.19.67:8888/getentiretagas), method=POST
        	Fetches the tags for the provided event ID
13.   /getinformation (http://10.29.19.67:8888/searchbyid), method=POST
        	Fetches video information for the provided event ID
14.   /gettimetags (http://10.29.19.67:8888/gettimetags), method=POST
        	Fetches tags for the provided event ID
15.   /tags (http://10.29.19.67:8888/searchbyid), method=PUT
        	Updates the provided tag information


Database

The databases are simple and consists of only tables (MySQL) or collections (MongoDB).
MySQL workbench (https://dev.mysql.com/downloads/workbench/) and MongoDB Management Studio community edition (http://mms.litixsoft.de/index.php?lang=de/) may be used to access the databases. 

The databases are running at 10.29.19.68. The username/password to access the MySQL database is video/video123 and there is no password to access MongoDB.

MySQL connection details:


MongoDB connection details:


MongoDB collection name (gridfs.gridfs.fs.files):





